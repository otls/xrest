# Xrest

express.js rest API starter

### Module Generator
This generator will create a module file and save it in a related folder according to the module its self. 
And will create a loader file (```export/import ``` ) which is located in src/app/ folder, the filename is according to
what module is generated such as ```routes.js```, ```controllers.js```, ```services.js```, ```requests.js```, ```middlewares.js```. It will be used for importing the module.
##### Generate Route 
Generate a route file and save in src/routes/**{RouteName.js}**
```sh
$ node ./xrest generate:route RouteName
```

##### Generate Controller 
Generate a controller file and save in src/controllers/**{ControllerName.js}**
```sh
$ node ./xrest generate:controller ControllerName
```

##### Generate Service 
Generate a service file and save in src/services/**{ServiceName.js}**
```sh
$ node ./xrest generate:service ServiceName
```

##### Generate Request (validation) 
Generate a request validation file and save in src/requests/**{RequestName.js}**
```sh
$ node ./xrest generate:requeste RequestName
```

##### Generate Middleware
Generate a middleware file and save in src/requests/**{MiddlewareName.js}**
```sh
$ node ./xrest generate:middleware MiddlewareName
```

### Module Sync
synchronizing the loader file which is located in src/app/ folder with the generated modules.
```sh
$ node ./xrest module:sync routes #sync module located in src/routes
$ node ./xrest module:sync controllers #sync module located in src/controllers
$ node ./xrest module:sync services #sync module located in src/services
$ node ./xrest module:sync requests #sync module located in src/requests
$ node ./xrest module:sync middlewares #sync module located in src/middlewares
```
