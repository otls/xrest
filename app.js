var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var routeInit = require('./xrest/route-init');
var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

routeInit(app);

// catch 404 and forward to error handler
app.use(async function (req, res, next) {
  const { Api } = require('./src/app/services');
  return (await Api.notFound()).sendJson(res);
});

module.exports = app;
