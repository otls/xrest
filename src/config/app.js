import path from 'path';

module.exports = {
    ROOT : process.cwd(),
    ROUTEPATH : path.join(process.cwd(), 'src', 'routes'),
    CONTROLLERPATH : path.join(process.cwd(), 'src', 'controllers'),
    SERVICEPATH : path.join(process.cwd(), 'src', 'services'),
    MODELPATH : path.join(process.cwd(), 'src', 'models'),
    REQUESTPATH: path.join(process.cwd(), 'src', 'requests'),
    APIPREFIX: '/api/v1',
    APPPATH: path.join(process.cwd(), 'src', 'app'),
    MIDDLEWAREPATH: path.join(process.cwd(), 'src', 'middlewares'),
}