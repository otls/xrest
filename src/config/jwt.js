exports.jwtSignOptions = {
    algorithm: 'HS256',
    expiresIn: '1d'
}