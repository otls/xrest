import {
    Jwt,
    Hash,
    Api
} from '../app/services';

import {
    users
} from '../app/models';

class AuthController {

    async login(req, res) {
        const { email, password } = req.body;
        const user = await users
            .scope('withPassword')
            .findOne({ where: { usr_email: email } });

        if (user && await Hash.compare(password, user.usr_password)) {

            const payload = {
                uid: user.id,
                email: user.usr_email,
                loginAt: new Date
            }
            const token = Jwt.make(payload);

            await Api.success('Login success!', { token: token });

        } else {
            await Api.unauthorized();
        }
        return await Api.sendJson(res);
    }

    async register(req, res) {
        let token = null;
        const data = {
            'usr_name': req.body.name,
            'usr_email': req.body.email,
            'usr_password': await Hash.make(req.body.password),
            'usr_phone': req.body.phone,
        };
        // const user = await users.create(data);
        // token = Jwt.generatePayload(user, true);
        return res.send(data);        
        return (await Api.success('success', token)).sendJson(res);
    }
}

export default new AuthController();