
import { Api } from '../app/services';
import { users } from "../app/models";

class ProfileController {

    async myProfile(req, res) {
        return users.findOne({where: {id: res.locals.user.uid}})
            .then(async data => {
                (await Api.success('Success', data)).sendJson(res);
            })
            .catch(async err => {
                (await Api.error('Something wrong')).sendJson(res);
            });
    }
}

export default new ProfileController()
