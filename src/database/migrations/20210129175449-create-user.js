'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      usr_name: {
        allowNull: false,
        type: Sequelize.STRING(100)
      },
      usr_email: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING(100)
      },
      usr_password: {
        allowNull: false,
        type: Sequelize.STRING
      },
      usr_phone: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true
      },
      usr_picture: {
        allowNull: true,
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('users');
  }
};