'use strict';


module.exports = {
  up: async (queryInterface, Sequelize) => {

    const bcrypt = require('bcrypt');
    const salt = await bcrypt.genSalt(parseInt(process.env.HASH_ROUND));
    const password = await bcrypt.hash('m.admin@mail.com123', salt)
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('users', [{
      usr_name: "M. Admin",
      usr_email: 'm.admin@mail.com',
      usr_phone: '628882849900',
      usr_password: password,
      created_at: new Date,
      updated_at: new Date
    }])


  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', {
      usr_email: "m.admin@gmail.com"
    },
      {}
    )
  }
};
