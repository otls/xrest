import { Jwt, Api } from '../app/services';

class AuthMiddleware {

    async handle(req, res, next) {
        let user = Jwt.payload(req, true);
        if (user) {
            res.locals.user = user;
            return next()
        }
        return (await Api.unauthorized()).sendJson(res);
    } 
}

export default new AuthMiddleware()
