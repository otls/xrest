'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Users.init({
    usr_name: DataTypes.STRING,
    usr_email: DataTypes.STRING,
    usr_password: DataTypes.STRING,
    usr_phone: DataTypes.STRING,
    usr_picture: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'users',
    underscored: true,
    defaultScope: {
      attributes: {
        exclude: ['usr_password']
      }
    },
    scopes: {
      withPassword: {
        attributes: {}
      },
      byId(id) {
        return {
          where: {
            id: id
          }
        }
      }
    }
  });
  return Users;
};