
import Joi from 'joi'

class LoginRequest {

    constructor(){

    }
    
    async authorized(req, res) {
      return true;
    }

    async rules(req, res) {
      return {
        email: Joi.string().max(255).email().required(),
        password: Joi.string().max(255).required()
      };
    }
}

export default new LoginRequest()
