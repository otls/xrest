import { users } from "../app/models";
import Joi from 'joi'
import { Validation } from "../app/services";

class RegistrationRequest {

  constructor() {
    
  }

  async authorized(req, res) {
    return true;
  }

  async rules(req, res) {
    return {
      name: Validation
        .string()
        .max(200)
        .required(),
      email: Validation
        .string()
        .max(200)
        .email()
        .required()
        .external(
          async (val) => await Validation.unique(val, 'users', 'usr_email', 'email')
        , 'email'),
      password: Validation
        .string()
        .min(8)
        .max(255)
        .required()
    }
  }
}

export default new RegistrationRequest()
