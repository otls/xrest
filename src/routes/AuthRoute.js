
import { Router } from 'express';
import { AuthController } from "../app/controllers";
import { Validation, Request } from "../app/services";

class AuthRoute {

    constructor() {
        this.prefix = '/auth';
    }

    routes() {

        let router = Router();
        router.post('/login', Request.validation('LoginRequest'), AuthController.login);
        router.post('/register', Request.validation('RegistrationRequest'), AuthController.register);

        return router;
    }
}

export default new AuthRoute();