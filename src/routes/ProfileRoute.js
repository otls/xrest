
import { Router } from 'express';
import { ProfileController } from '../app/controllers';
import { Request } from "../app/services";

class ProfileRoute {

    constructor() {
        this.prefix = '/profile';
        this.middlewares = Request.middleware(['AuthMiddleware']);
    }

    routes() {

        let router = Router();
        router.get('/', ProfileController.myProfile);

        return router;
    }
}

export default new ProfileRoute()
