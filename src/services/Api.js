class Api {
    
    constructor() {
        this._initResponse();
    }

    _initResponse() {
        this.response = {
            'success': true,
            'code': 200,
            'message': 'success',
            'data': []
        };
    }

    _setResponse(code, success, message, data = null, appends = null) {
        this.response.code = code;
        this.response.success = success;
        this.response.message = message;
        data ? this.response.data = data : delete this.response.data;
        appends ? this.response = { ...this.response, ...appends } : null;
    }

    async success(message = "success", data = null, appends = [], code = 200) {
        this._setResponse(code, true, message, data, appends);
        return this;
    }

    async error(message = "failed", data = [], appends = [], code = 500) {
        this._setResponse(code, true, message, data, appends);
        return this;
    }
    async unauthorized() {
        this._setResponse(401, false, "Unauthorized");
        return this;
    }

    async validationError(errors) {
        this._setResponse(422, false, "Unprocessable Entity", null, { errors: errors });
        return this;
    }

    async forbidden() {
        this._setResponse(403, false, 'You are forbidden!');
        return this;
    }

    async notFound() {
        this._setResponse(404, false, 'You are lost, dude!');
        return this;
    }

    async sendJson(res, appends = []) {
        if (appends.length > 0) {
            this.response = { ...this.response, ...appends }
        }
        const response = this.response;
        this._initResponse();
        return res.status(response.code).json(response);
    }
}

export default new Api();