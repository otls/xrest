import Bcrypt from "bcrypt";

class Hash {
    async compare(plaintext, hash) {
        return await Bcrypt.compare(plaintext, hash);
    }

    async make(plaintext) {
        const saltRounds = parseInt(process.env.HASH_ROUND) || 10;
        return await Bcrypt.hash(plaintext, saltRounds);
    }
}

export default new Hash();
