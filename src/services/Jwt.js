import Jsonwebtoken from 'jsonwebtoken';
import { jwtSignOptions } from "../config/jwt";

class Jwt {

    make(payload) {
        const secret = process.env.JWT_SECRET;
        const token = Jsonwebtoken.sign(payload, secret, jwtSignOptions);
        return token;
    }

    payload(token, extract = false) {
        if (extract) {
            token = this.extract(token);
        }
        const secret = process.env.JWT_SECRET;
        return Jsonwebtoken.verify(token, secret, (err, decoded) => {
            return !err ? decoded : false;
        })
    }

    extract(req) {
        let token = req.header('Authorization');
        if (token) {
            token = token.split(" ").pop();
        } else {
            token = false;
        }
        return token;
    }

    generatePayload(user, token = false) {
        let data = {
            uid: user.id,
            email: user.usr_email,
            loginAt: new Date
        }
        if (token) {
            data = this.make(data);
        }
        return data;
    }
}

export default new Jwt();