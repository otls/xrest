import { MIDDLEWAREPATH } from "../config/app";
import path from 'path';


const Middleware = (middleware) => {
    const midModule = require(path.join(MIDDLEWAREPATH, middleware));
    return midModule.handle;
}

export default Middleware;
