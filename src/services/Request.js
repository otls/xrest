import Joi from 'joi';
import path from 'path';
import { APPPATH } from "../config/app";
import { Api as apiservice } from '../app/services';
import { users } from "../app/models";

class Request {

    validation (validation) {
        validation = require(path.join(APPPATH, 'requests'))[validation];
        const run = async (req, res, next) => {
            return await this.#runValidation(req, res, next, validation);
        }
        return run;
    }

    async #runValidation (req, res, next, validation) {

        if (typeof validation.authorized === 'function' && ! await validation.authorized(req, res)) {
            return (await apiservice.forbidden()).sendJson(res);
        }   
        
        return Joi
            .object(await validation.rules(req, res))
            .validateAsync(req.body)
            .then(() => {
                return next();
            })
            .catch(async err => {
                // console.log(err);return
                let errors = {};
                err.details.map((val, i) => {
                    errors[val.context.key] = val.message;
                });
                return (await apiservice.validationError(errors)).sendJson(res);
            });
    }

    middleware(midnames) {
        const MiddModules = require(path.join(APPPATH, 'middlewares'));

        if (!Array.isArray(midnames)) {
            return MiddModules[midnames].handle;
        }

        let midClass = null;
        let middlewares = [];

        for (let i = 0; i < midnames.length; i++) {
            midClass = midnames[i];
            middlewares.push(MiddModules[midClass].handle);
        }
        return middlewares;
    }

    async uniqueValidation(val, helper) {
        const account = await users.findOne({ where: { usr_email: val } });
        if (account) {
            return helper.error('any.invalid');
        }
        console.log('');
    }
}

export default new Request()
