import Joi from 'joi';

Joi.unique = async (value, table, column, label = '') => {

    let model = require(process.cwd() + '/src/app/models')[table];
    let where = { [column]: value };
    let data = await model.findOne({ where: where });
    let joi = Joi;
    
    label = label ? label : column;
    data = {
        [label]: data[column]
    }
    console.log(data);

    const { error } = joi.object({
            [label]: joi.invalid(data[label]).label(label).messages({
                'any.invalid': 'Data alredy in use'
            })
        })
        .validate(data);

    if (error) {
        throw new joi.ValidationError('error', error.details);
    }
}

export default Joi;