const generator = require('./xrest/generators');
const moduleSync = require('./xrest/module/sync');


const argv = process.argv;
const command = argv[2];

switch (command) {
    case 'generate:controller':
        generator(argv);
        break;
    
    case 'generate:request':
        generator(argv);
        break;
    
    case 'generate:service':
        generator(argv);
        break;
    
    case 'generate:route':
        generator(argv);
        break;
    
    case 'generate:middleware':
        generator(argv);
        break;
    
    case 'module:sync':
        moduleSync(argv);
        break;
    default:
        console.log(process.argv);
        break;
}