const fs = require('fs');

const script = (moduleName, moduleClass) => {
    let moduleTemplate = require(`./templates/${moduleName}`);
    moduleClass = moduleTemplate.replace(/xxxchangeAblexxx/g, moduleClass)
    return moduleClass;
}

const addFiles = (path, filename, scriptText) => {
    let srcFullPath = `./src/${path}/${filename}.js`;
    let appFullPath = `./src/app/${path}.js`

    if (fs.existsSync(srcFullPath)) {
        console.error('File already exists!');
        return;
    }

    fs.writeFileSync(srcFullPath, scriptText, (err) => {
        if (err) throw err;
    });

    if (!fs.existsSync(appFullPath)) {
        fs.writeFileSync(appFullPath, "", (err) => {
            if (err) throw err;
        });
    }

    let exportImportText = `export { default as ${filename} } from '../${path}/${filename}';\n`;
    fs.appendFileSync(appFullPath, exportImportText, (err) => {
        if (err) throw err
    });
    console.log(`${srcFullPath} created successfully`);
}

const handler = (name) => {

    let className = name[3].split("/").pop();
    let order = name[2].split(':').pop();

    switch (order) {
        case 'controller':
            scriptText = script('Controller',className)
            path = "controllers";
            break;
        
        case 'service':
            path = "services";
            scriptText = script('Service', className)
            break;
        
        case 'route':
            path = "routes";
            scriptText = script('Route', className)
            break;
        
        case 'request':
            path = "requests";
            scriptText = script('Request', className)
            break;
        
        case 'middleware':
            path = "middlewares";
            scriptText = script('Middleware', className)
            break;
        
        default:
            console.log("No command found");
            return false;
            break;
    }
        
    addFiles(path, className, scriptText);
}

module.exports = handler;