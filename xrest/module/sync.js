const fs = require('fs');
const path = require('path');

function searcher(dir, moduleList = "", modulePath) {
    fs.readdirSync(dir).forEach(file => {
        let fullPath = path.join(dir, file);
        if (fs.lstatSync(fullPath).isDirectory()) {
            searcher(fullPath, moduleList, modulePath);
        } else {
            let name = fullPath.split(/.*[\/|\\]/)[1].replace('.js', "");
            if (fullPath.slice(-3) === '.js' && name) {
                moduleList += `export { default as ${name} } from '../${modulePath}/${name}';\n`;
            }
        }
    });
    return moduleList;
}

const moduleSync = (argvs) => {
    let modulePath = argvs[3];
    let filelocation = path.join(process.cwd(), 'src', modulePath);
    let fullpathfile = path.join(process.cwd(), 'src', 'app', modulePath + ".js");

    let moduleList = "";
    moduleList = searcher(filelocation, moduleList, modulePath);

    fs.writeFileSync(fullpathfile, moduleList, (err) => {
        if (err) {
            throw err;
        }
    });
    console.log("Sync module success!");
}

module.exports = moduleSync;