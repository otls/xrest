import * as routers from '../../src/app/routes';

module.exports = (app) => {
    for (const k in routers) {
        if (Object.hasOwnProperty.call(routers, k)) {

            let route = routers[k];
            let prefix = (route.prefix ? route.prefix : '/');

            if (route.middlewares) {
                app.use(prefix, route.middlewares, route.routes());
            } else {
                app.use(prefix, route.routes());
            }
        }
    }
}